function filter(array, cb) 
{
    if(array.length===0)
    {
        return [];
    }
    let filter_array = [];
    for (let index = 0; index < array.length; index++) 
    {
        if(cb(array[index],index,array)===true)
        {
            filter_array.push(array[index]);
        }     
    }
    return filter_array;

};

module.exports = filter;