function flatten(array,depth=1)
{
    if(array.length==0)
    {
        return [];
    }
    let flatten_array=[];
    for(let index=0;index<array.length;index++)
    {
        if(Array.isArray(array[index]) && depth>=1)
        {
            flatten_array=flatten_array.concat(flatten(array[index],depth-1));
        }
        else
        {
            if(array[index]===undefined || array[index]===null)
            {
                continue;
            }
            else
            {
                flatten_array.push(array[index]);
            }
            
        }
    }
    return flatten_array;
}
module.exports=flatten;