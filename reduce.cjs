function reduce(array, cb, initial_value) 
{
    if(array.length===0)
    {
        return [];
    }
    let index;
    let accu_mulator;
    if(initial_value===undefined)
    {
        accu_mulator=array[0];
        index=1;
    }
    else
    {
        index=0;
        accu_mulator=initial_value;
    }
    while(index < array.length)
    {
        accu_mulator = cb(accu_mulator, array[index],index,array);
        index++;
    }
    return accu_mulator;

};

module.exports = reduce;
