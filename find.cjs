function find(array,cb) 
{
    if(array.length===0)
    {
        return [];
    }
    for(let index=0;index<array.length;index++)
    {
        if(cb(array[index],index,array))
        {
            return array[index];
        }
    }
    return undefined;
};

module.exports = find;
