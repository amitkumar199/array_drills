function map(array, cb) 
{
    if(array.length===0)
    {
        return [];
    }
    let new_array = [];
    for (let index = 0; index < array.length; index++) 
    {
        new_array.push(cb(array[index], index,array));
    }
    return new_array;

};

module.exports = map;
